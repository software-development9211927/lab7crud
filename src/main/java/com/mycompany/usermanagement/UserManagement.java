/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.usermanagement;

import java.util.ArrayList;

/**
 *
 * @author Pond
 */
public class UserManagement {

    public static void main(String[] args) {
        User admin = new User("admin", "Administrator", "Pass@1234", 'M', 'A');
        User user1 = new User("user1", "User 1", "Pass@1234", 'M', 'U');
        User user2 = new User("user2", "User 2", "Pass@1234", 'M', 'U');

        ArrayList<User> userList = new ArrayList<User>();
        userList.add(admin);
        System.out.println(userList.get(userList.size() - 1) + "list size = " + userList.size());
        userList.add(user1);
        System.out.println(userList.get(userList.size() - 1) + "list size = " + userList.size());
        userList.add(user2);
        System.out.println(userList.get(userList.size() - 1) + "list size = " + userList.size());

    }
}
